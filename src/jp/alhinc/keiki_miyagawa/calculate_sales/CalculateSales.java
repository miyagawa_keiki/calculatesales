package jp.alhinc.keiki_miyagawa.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculateSales {
	public static void main(String[] args) {

		// 支店定義dataと売上dataを参照

		Map<String, String> branchList = new HashMap<>();
		Map<String, Long> salesMoney = new HashMap<>();

		// ファイルの読み込み

		File folder = new File(args[0]);
		File files[] = folder.listFiles();
		BufferedReader br = null;

		File totalSales = new File(args[0], "branch.out");

		BufferedWriter bw = null;

		try {

			// 1行ずつファイルを呼び出す

			File file = new File(args[0], "branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// ファイルの中身を,で区切って抽出

			String display;
			while ((display = br.readLine()) != null) {
				String[] contents = display.split(",");

				// ファイルの中身が"支店コード3桁,店名"出ない場合のエラー検索

				if (!contents[0].matches("^[0-9]{3}") || contents.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					fr.close();
					return;
				}

				// Mapにsetするkeyとvalue

				branchList.put(contents[0], contents[1]);
				salesMoney.put(contents[0], 0L);

			}

			// ファイルが連番になっているか確認するために置いた変数

			int earnings = 0;
			List<String> rcdFiles = new ArrayList<>();

			// 支店売上ファイルの検索を行う

			for (int i = 0; i < files.length; i++) {
				String book = files[i].getName();

				// 8桁の数字と.rcdが付いているファイルの検索

				Pattern p = Pattern.compile("^[0-9]{8}.rcd$");
				Matcher m = p.matcher(book);

				// 検索できた売上ファイルの読み込み

				while (m.find()) {
					String searchResults = m.group();
					rcdFiles.add(searchResults);
				}
			}

			for (int i = 0; i < rcdFiles.size(); i++) {
				File file1 = new File(folder, rcdFiles.get(i));
				FileReader fr1 = new FileReader(file1);
				br = new BufferedReader(fr1);

				String code;
				String sale;

				// 1個目の読み込みが支店番号、2個目の読み込みが売上金額

				code = br.readLine();
				sale = br.readLine();

				if (!branchList.containsKey(code)) {
					System.out.println(rcdFiles.get(i) + "の支店コードが不正です");
					fr1.close();
					return;
				}

				// ファイル内の3行目に文字が書かれていないかの確認（書いてあればエラー）

				String thirdLine = br.readLine();
				if (thirdLine != null) {
					System.out.println(rcdFiles.get(i) + "のフォーマットが不正です");
					fr1.close();
					return;
				}

				// 売上金額をlong型にして値を代入
				// mapにsetする ※mapの値に ＋result がされている

				long result = Long.parseLong(sale);
				salesMoney.put(code, salesMoney.get(code) + result);

				// ファイル名が連番になっているかどうかの確認（駄目ならエラー）

				String[] separation = rcdFiles.get(i).split(".rcd");
				int SerialNumber = Integer.parseInt(separation[0]);
				int Cumulative = (SerialNumber - earnings);

				if ((Cumulative != 1) && i != 0) {
					System.out.println("売上ファイル名が連番になっていません");
					fr1.close();
					return;
				}

				earnings = SerialNumber;

			}

			// 上記と同じ処理で繰り返すが、出力をファイル内に

			BufferedWriter write = new BufferedWriter(new FileWriter(totalSales));
			bw = new BufferedWriter(write);

			try {

				for (Entry<String, String> get : branchList.entrySet()) {
				String codeName = get.getKey();
				String saleValue = get.getValue();

				bw.write(codeName + "," + saleValue + "," + salesMoney.get(codeName));
				bw.newLine();

				// 支店別合計金額が8桁を超えないか確認（超えたらエラー）

				long over = salesMoney.get(codeName);
					if (over > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					write.close();
					return;
					}
				}
			} finally {
				bw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}